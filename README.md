<div style="text-align: center">
    <img src="logo.png" alt="plethora logo">
    <p>a nagios-like application aiming at the IOT world!</p>
</div>

<hr>

# | REST Api

This repository contains the REST Api project.
It is developed using the [Nodejs][node_url] platform and transpiled from
[TypeScript][ts_url]. It uses [restify][restify_url] as main REST library.
All the tests are written using [jest][jest_url] as an assertion framework
and [supertest][supertest_url] to assert HTTP requests.

[node_url]: https://nodejs.org
[ts_url]: https://typescriptlang.org
[restify_url]: http://restify.com/
[jest_url]: http://facebook.github.io/jest/
[supertest_url]: https://github.com/visionmedia/supertest

## Installation

to initialize the environment, you should launch the following commands

    $ npm install

verify that everything is ok by launching the tests

    $ npm run test

To launch the server you need to copy `.env.example` to `.env`, then do the following

    $ npm run build
    $ node dist

it should output the port (`1337` by default).

From now, your environment is operational.

## Development

This project uses editorconfig to standardize the end of lines and tabulation sizes,
please install a plugin according to your text editor, following the
[official documentation][editorconfig_url].

In addition, this project uses a modified [airbnb standard][airbnb_standard],
to follow this core guideline install the appropriate [tslint][tslint_url]
plugin for your editor. When a rule is ignored, please add a comment inside
the source code explaining why. As a general rule, no red line should stay.

Once your editor is configured, you should be able to develop in typescript.
To run your code while your editing it, run

    $ npm run dev

which will launch nodemon and the typescript compiler together.
From time to time, when your browser, curl or postman does not answer check the terminal
where these processes run, there should be an error there.

As a convenience, when you add lines to the documentation, use curl, which is
a good way to describe a HTTP Request. Everything should be documented using
[Apidoc][apidoc_url], and tested using jest and supertest.


### Tests

A test starts by adding a file inside
the `test/` folder. Every test file **MUST** follow the convention

    [filename].test.ts

Here is an example test for a route returning hello world

```ts
/** env jest */

import * as request from 'supertest';
import { Server, createServer } from 'restify';

import serverFactory from '../src/routes/serverFactory';

let server: Server;

beforeAll(() => {
    server = serverFactory(
        createServer(),
    );
});

describe('/hello', () => {
    it('should return "hello world!" as JSON', (done) => {
        request(server)
            .get('/hello')
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
                if (err) return done(new Error('err should be undefined, received' + err));
                expect(res.body.hello).toBe('world!');
                done();
            });
    });
});
```

> **NOTE**: It should be possible to test only the controller, but testing all
> the route is far more convenient, since you don't need to mock anything

### Conventions and Best Practices

Moreover, there are some special conventions.
All the files describing routes should be named folowing the convention

    src/routes/[route]Route.ts

Everytime you add a new route, you should declare it inside the `routes.ts` file

```ts
import myRoute from './myRoute';
const routes = [
    myRoute,
];
export default routes;
```

this constant will then be used by the `serverFactory`. For your information, every
file inside `routes/` is part of the [decorator pattern][deco_url] used to
instantiate every route. That's why every new `*Route.ts` file should export a
function taking a restify `Server` as input and returning a restify `Server`,
e.g. the following function

```ts
const helloRoutes = (server: Server): Server => {
    /**
     * @api {get} /hello Say Hello World!
     */
    server.get('/hello', HelloController.sayHello);
    return server;
};
```

Remember that all the logic of the said route should live inside a `*Controller`
class in the `src/controllers` folder. All the functions of the controller are static
and sould take a signature of a restify route handler.

```ts
import { Request, Response, Next } from 'restify';

export default class HelloController {
    static sayHello(req: Request, res: Response, next: Next): void {
        res.json({
            hello: 'world!',
        });
        return next();
    }
}
```

[editorconfig_url]: http://editorconfig.org/#download
[airbnb_standard]: https://github.com/airbnb/javascript
[tslint_url]: https://palantir.github.io/tslint/
[apidoc_url]: http://apidocjs.com/
[deco_url]: https://en.wikipedia.org/wiki/Decorator_pattern

## Contributors

+ [Constant Deschietere](mailto:constant.deschietere@cpe.fr)
+ [Rudy Deal](mailto:rudy.deal@cpe.fr)
+ [Maxime Collot](mailto:maxime.collot@cpe.fr)
+ [Florian Croisot](mailto:constant.deschietere@cpe.fr)
+ [Jordan Quagliatini](mailto:jordan.quagliatini@cpe.fr)
