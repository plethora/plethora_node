import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();
if (error) {
    throw error;
}

import { MongoClient, Db } from 'mongodb';

import * as restify from 'restify';

import serverFactory from './routes/serverFactory';

MongoClient.connect(process.env.MONGO_URL || 'mongodb://localhost:27017/PLETHORA')
    .then((db: Db) => {
        const server = serverFactory(
            restify.createServer(),
            db,
        );

        server.listen(process.env.APP_PORT, () => {
            console.log(':%s', process.env.APP_PORT);
        });
    });
