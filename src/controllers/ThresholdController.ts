import { Response, Next } from 'restify';
import { InternalServerError } from 'restify-errors';
import { Db } from 'mongodb';
import { Request } from '../types';

import { getPercentageThreshold } from '@plethora/utils';

export default class ThresholdController {
    static getThreshold(req: Request, res: Response, next: Next): void {
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }

        getPercentageThreshold(
            req.db,
            req.params.deviceid,
            req.params.moduleid,
        ).then((threshold: number) => {
            res.json({
                threshold,
            });
            return next();
        })
        .catch(() => {
            return next(
                new InternalServerError('Something bad happened --\''),
            );
        });
    }
}
