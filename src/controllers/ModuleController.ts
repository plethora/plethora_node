import { Response, Next } from 'restify';
import { InternalServerError, BadRequestError } from 'restify-errors';
import { Request } from '../types';



const messageToData = (message: any) => ({
    timestamp: message.timestamp,
    data: message.data.value,
});


export default class ModuleController {

    static getMonitoringNow(req: Request, res: Response, next: Next): void {
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }
        req.db.collection('messages')
            .findOne({ device_id: req.params.deviceid, module_id: req.params.moduleid }, {
                fields: { timestamp: 1, 'data.value': 1, _id: 0 },
                sort: { timestamp: -1 },
            })
            // récupération du dernier message correspondant au device et au module.
            .then((data) => {
                if (!data) {
                    res.json({});
                    return next();
                }

                res.json(
                    messageToData(data),
                );
                return next();

            },
        );
    }

    static getMessagesTimed(req: Request, res: Response, next: Next) {
        // FIXME this part should not be repeated
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }

        const from = Number(req.query.from);
        const to = Number(req.query.to);

        if (isNaN(from) ||
            isNaN(to)) {
            return next(
                new BadRequestError('There is a mistake on the parameter "to" and "from"'),
            );
        }

        if (from > to) {
            return next(
                new BadRequestError('from should be lower than to'),
            );
        }

        const findArgument = {
            device_id: req.params.deviceid,
            module_id: req.params.moduleid,
            timestamp: {
                $gte: from,
                $lte: to,
            },
        };

        const findOptions = {
            fields: {
                timestamp: 1,
                'data.value': 1,
                _id: 0,
            },
        };
        req.db.collection('messages')
            .find(findArgument, findOptions)
            .toArray()
            .then((data) => {
                if (data.length === 0) {
                    res.json([]);
                    return next();
                }

                res.json(
                    data.map(messageToData),
                );
                return next();
            });


    }
}


