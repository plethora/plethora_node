import { Response, Next } from 'restify';
import { InternalServerError } from 'restify-errors';
import { Request } from '../types';

export default class DeviceController {
    static getDevices(req: Request, res: Response, next: Next): void {
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }

        req.db.collection('devices').find({},{ _id: 0, modules: 0 }).toArray()
            .then((data) => {
                res.json(data);
                return next();
            });
    }

    static getDevice(req: Request, res: Response, next: Next) : void {
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }

        req.db.collection('devices')
        .findOne({ device_id:req.params.deviceId },
                 { fields:{ _id: 0 } })
            .then((dataDevise) => {
                if (!req.db) {
                    return next(
                        new InternalServerError('Cannot access the database'),
                    );
                }
                req.db.collection('messages')
                .find({ device_id:req.params.deviceId },{
                    fields: { _id: 0 , 'data.key': 1 },
                    sort: { timestamp: -1 },
                    limit:  1000  })
                    // TOFIX on sprint 4 : by an internal request on mongo
                    // in order to select all the different module_id
                    // for one devise on the last 30 minutes
                .toArray()
                .then((dataModules) => {

                    const uniqueModules = Object.keys(
                        dataModules
                            .map(dataModule => dataModule.data.key)
                            .reduce(
                                (agg, messageKey) => ({
                                    ...agg,
                                    [messageKey]: agg[messageKey]
                                        ? agg[messageKey] + 1
                                        : 0,
                                }),
                                {},
                            ),
                    );

                    res.json({
                        device_id : dataDevise.device_id,
                        hostname : dataDevise.hostname,
                        ip : dataDevise.ip,
                        status : dataDevise.status,
                        modules : uniqueModules,
                    });
                });
                return next();
            });
    }
}
