import { Response, Next } from 'restify';
import { InternalServerError, BadRequestError } from 'restify-errors';
import { Request } from '../types';

const leftpad = (nb: number) => nb < 10
        ? `0${nb}`
        : `${nb}`;

const timestampToHHMM = (timestamp: number) => {
    const date = new Date(timestamp);
    return `${leftpad(date.getHours())}:${leftpad(date.getMinutes())}`;
};

const messageToPlotData = (message: any) => ({
    x: timestampToHHMM(message.timestamp),
    y: Number(message.data.value).toFixed(2),
});

const messageToPlotHeartbeat = (message: any) => ({
    x: timestampToHHMM(message.timestamp),
    y: (message.data.value === 'UP') ? 1 : 0,
});

const messageIsPlotable = (message: any) => {
    const val = Number(message.data.value);
    return !isNaN(val);
};

export default class PlotController {

    static getNowPlotMessage(req: Request, res: Response, next: Next): void {
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }
        req.db.collection('messages')
            .findOne({ device_id: req.params.deviceid, module_id: req.params.moduleid }, {
                fields: { timestamp: 1, 'data.value': 1, _id: 0 },
                sort: { timestamp: -1 },
            })
            // récupération du dernier message correspondant au device et au module.
            .then((data) => {
                if (!data) {
                    res.json({});
                    return next();
                }

                if (!messageIsPlotable(data)) {
                    if (data.data.value === 'UP') {
                        res.json(
                            messageToPlotHeartbeat(data),
                        );
                        return next();
                    }
                    return next(
                        new BadRequestError(
                            `messages from ${req.params.moduleid} are not plotable!`,
                        ),
                    );
                }

                res.json(
                    messageToPlotData(data),
                );
                return next();
            });
    }

    static getPlotMessageTimed(req: Request, res: Response, next: Next) {
        // FIXME this part should not be repeated
        if (!req.db) {
            return next(
                new InternalServerError('Cannot access the database'),
            );
        }

        const from = Number(req.query.from);
        const to = Number(req.query.to);

        if (isNaN(from) ||
            isNaN(to)) {
            return next(
                new BadRequestError('There is a mistake on the parameter "to" and "from"'),
            );
        }

        if (from > to) {
            return next(
                new BadRequestError('from should be lower than to'),
            );
        }

        const findArgument = {
            device_id: req.params.deviceid,
            module_id: req.params.moduleid,
            timestamp: {
                $gte: from,
                $lte: to,
            },
        };
        const findOptions = {
            fields: {
                timestamp: 1,
                'data.value': 1,
                _id: 0,
            },
        };
        req.db.collection('messages')
            .find(findArgument, findOptions)
            .toArray()
            .then((data) => {
                if (data.length === 0) {
                    res.json([]);
                    return next();
                }

                if (!messageIsPlotable(data[0])) {
                    if (data[0].data.value === 'UP') {
                        res.json(
                            data.map(messageToPlotHeartbeat),
                        );
                        return next();
                    }
                    return next(
                        new BadRequestError(
                            `messages from ${req.params.moduleid} are not plotable!`,
                        ),
                    );
                }

                res.json(
                    data.map(messageToPlotData),
                );
                return next();
            });
    }
}
