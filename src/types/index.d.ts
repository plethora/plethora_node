import { Request as RestifyRequest} from 'restify';
import { Db } from 'mongodb';

export interface Request extends RestifyRequest {
    db?: Db;
}
