import { Server } from 'restify';
import { Db } from 'mongodb';

import CorsController from '../controllers/CorsController';

const corsMiddleware = (server: Server, db?: Db): Server => {
    server.use(CorsController.corsDefaultMiddleware);
    return server;
};

export default corsMiddleware;
