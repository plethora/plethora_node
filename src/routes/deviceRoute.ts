import { Server } from 'restify';
import { Db } from 'mongodb';

import DeviceController from '../controllers/DeviceController';

const deviceRoutes = (server: Server, db?: Db): Server => {
    /**
     * @api {get} /devices send the IP with ID
     */
    server.get('/devices', DeviceController.getDevices);

    /**
     * @api {get} /devices/:deviceid Send the info linked to the device with the ID deviceid
     */
    server.get('/devices/:deviceId', DeviceController.getDevice);
    return server;
};

export default deviceRoutes;
