import { Server, Response, Next, plugins as restifyPlugins } from 'restify';
import { Db } from 'mongodb';
import { Request } from '../types';

import routes from './routes';

const serverFactory = (server: Server, db: Db): Server => {
    server.pre((req: Request, res: Response, next: Next): void => {
        req.db = db;
        return next();
    });

    server.use(restifyPlugins.queryParser());

    return routes.reduce(
        (s, route) => route(s),
        server,
    );
};


export default serverFactory;
