import { Server, Response, Next } from 'restify';
import { Request } from '../types';

/**
 * This middleware basically read the router configuration to explicitly set the
 * `Access-Control-Allow-Methods` HTTP header for preflight requests.
 * It is a safer way to ensure that a route exists than simply specifying
 *
 * ```
 * sever.opts(new RegExp('.*'), (req, res, next) => {
 *   res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE,HEAD');
 *   return next();
 * });
 * ```
 *
 * It doesn't override existing OPTIONS route when specified
 */
const optsMiddleware = (server: Server): Server => {
    // the `any` typecast is a workaround to remove anoying error messages on build!
    const methodsOfPaths = (server.router as any).reverse;
    Object.keys(methodsOfPaths)
        .forEach((path: string) => {
            // we don't want to override existing OPTIONS handlers
            if (methodsOfPaths[path].some((k: string) => k === 'OPTIONS')) {
                return;
            }

            server.opts(new RegExp(path), (req: Request, res: Response, next: Next) => {
                res.header(
                    'Access-Control-Allow-Methods',
                    methodsOfPaths[path].filter((m: string) => m !== 'OPTIONS').join(','),
                );
                res.send(204);
                return next();
            });
        });
    return server;
};

export default optsMiddleware;
