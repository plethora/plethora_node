import { Server } from 'restify';
import PlotController from '../controllers/PlotController';

const plotRoute = (server: Server): Server => {
    server.get('/devices/:deviceid/:moduleid/plot', PlotController.getNowPlotMessage);
    server.get('/devices/:deviceid/:moduleid/plot/time', PlotController.getPlotMessageTimed);

    return server;
};

export default plotRoute;
