import deviceRoute from './deviceRoute';
import moduleRoute from './moduleRoute';
import plotRoute from './plotRoute';
import thresholdRoute from './thresholdRoute';

import corsMiddleware from './corsMiddleware';
import optsMiddleware from './optsMiddleware';

const routes = [
    // this middleware should always be at the top!
    corsMiddleware,
    deviceRoute,
    moduleRoute,
    plotRoute,
    thresholdRoute,
    // optsMiddleware should always be the last route!
    optsMiddleware,
];

export default routes;
