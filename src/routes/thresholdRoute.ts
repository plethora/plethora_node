import { Server } from 'restify';
import { Db } from 'mongodb';

import ThresholdController from '../controllers/ThresholdController';

const thresholdRoutes = (server: Server, db?: Db): Server => {
    /**
     * @api {get} /threshold/:deviceid/:moduleid send the threshold for the ID
     */
    server.get('/threshold/:deviceid/:moduleid', ThresholdController.getThreshold);
    return server;
};

export default thresholdRoutes;
