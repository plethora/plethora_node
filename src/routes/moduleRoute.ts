import { Server } from 'restify';
import { Db } from 'mongodb';

import ModuleController from '../controllers/ModuleController';

const moduleRoutes = (server: Server, db?: Db): Server => {
    /**
     * @api {get} /devices/module/:devicesid/:moduleid/ récupération des dernières
     * données d'un module pour un device donné
     */
    server.get('/devices/:deviceid/:moduleid', ModuleController.getMonitoringNow);
    server.get('/devices/:deviceid/:moduleid/time', ModuleController.getMessagesTimed);
    return server;
};
export default moduleRoutes;
