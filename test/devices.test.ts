/** env jest */

import * as request from 'supertest';
import { Server, createServer } from 'restify';

import serverFactory from '../src/routes/serverFactory';
import { MongoClient, Db } from 'mongodb';

import { v4 } from 'uuid';

let gDb: Db;
let server: Server;

const firstUUID = v4();

beforeAll(() =>
    MongoClient.connect('mongodb://localhost:27017/PLETHORA_TEST')
        .then((db: Db) => {
            gDb = db;
            return db.collection('devices')
                .insert({
                    device_id: firstUUID,
                    status: true,
                    ip: '127.0.0.1',
                    hostname: `hostname#${firstUUID}`,
                });
        })
        .then(() =>
            gDb.collection('messages')
                .insert({
                    device_id: firstUUID,
                    module_id: 'HOSTNAME_MODULE',
                    timestamp: new Date().getTime(),
                    data: {
                        key: 'HOSTNAME',
                        value: `hostname#${firstUUID}`,
                    },
                }),
        )
        .then(() => {
            server = serverFactory(
                createServer(),
                gDb,
            );
        }),
);

describe('/devices', () => {
    it('should return a list of devices', (done) => {
        request(server)
            .get('/devices')
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
                if (err) return done(new Error('err should be undefined, received' + err));
                expect(res.body.length).toBe(1);
                expect(res.body[0]).toEqual({
                    device_id: firstUUID,
                    status: true,
                    ip: '127.0.0.1',
                    hostname: `hostname#${firstUUID}`,
                });
                done();
            });
    });
});

describe(`/devices/${firstUUID}`, () => {
    it('should return a specific device with a list of modules', (done) => {
        request(server)
            .get(`/devices/${firstUUID}`)
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
                if (err) return new Error('err should be undefined, received ' + err);

                expect(res.body).toEqual({
                    device_id: firstUUID,
                    status: true,
                    ip: '127.0.0.1',
                    hostname: `hostname#${firstUUID}`,
                    modules: [
                        'HOSTNAME',
                    ],
                });
                done();
            });
    });
});

afterAll(() => {
    return gDb.collection('devices')
        .remove({})
        .then(() => gDb.collection('messages').remove({}))
        .then(() => {
            gDb.close(true);
        });
});
