/** env jest */

import * as request from 'supertest';
import { Db } from 'mongodb';
import { Server, createServer } from 'restify';

import serverFactory  from '../src/routes/serverFactory';

let gServer: Server;

beforeAll(() => {
    gServer = serverFactory(createServer(), ({} as Db));
});

describe('CORS Headers', () => {
    it('should send CORS headers for OPTIONS request', (done) => {
        request(gServer)
            .options('/devices')
            .set('Access-Control-Request-Headers', 'x-origin')
            .set('Content-Type', 'application/json')
            .expect(204)
            .expect('Access-Control-Allow-Origin', '*')
            .expect('Access-Control-Allow-Credentials', 'true')
            .expect('Access-Control-Allow-Headers', 'content-type,x-origin')
            .end((err, res) => {
                if (err) done(new Error('err should be empty, received: ' + err));

                done();
            });
    });
});
