/** env jest */

import * as request from 'supertest';
import { MongoClient, Db } from 'mongodb';
import { Server, createServer } from 'restify';
import { getPercentageThreshold } from '@plethora/utils';
import { v4 } from 'uuid';

import serverFactory from '../src/routes/serverFactory';

const getRandom = () => Math.floor(
    Math.random() * 100,
);

let gServer: Server;
let gDb: Db;

beforeAll(() => {
    return MongoClient.connect('mongodb://localhost:27017/PLETHORA_TEST')
        .then((db: Db) => {
            gDb = db;
            gServer = serverFactory(
                createServer(),
                db,
            );
        });
});

describe('/threshold', () => {
    afterEach(() => {
        return gDb.collection('messages').remove({});
    });

    it('should return 100 when less than 30 entries are in the db', (done) => {
        const deviceId = v4();
        const now = new Date().getTime();

        const values = Array.apply(null, Array(20))
            .map((_: any, i: number) => ({
                device_id: deviceId,
                module_id: 'MODULE_ID',
                timestamp: now - i,
                data: {
                    key: 'MODULE',
                    value: getRandom(),
                },
            }));
        gDb.collection('messages')
            .insertMany(values)
            .then(() => {
                request(gServer)
                    .get('/threshold/${deviceId}/MODULE_ID')
                    .expect(200)
                    .end((err, res) => {
                        if (err) {
                            return done(new Error('expected error to be undefined, got: ' + err));
                        }

                        expect(res.body.threshold).toBe(100);
                        done();
                    });
            });
    });

    it('should return the avg value when more than 30 entries are in the db', (done) => {
        const deviceId = v4();
        const now = new Date().getTime();

        const values = Array.apply(null, Array(31))
            .map((_: any, i: number) => ({
                device_id: deviceId,
                module_id: 'MODULE_ID',
                timestamp: now - i,
                data: {
                    key: 'MODULE',
                    value: getRandom(),
                },
            }));
        gDb.collection('messages')
            .insertMany(values)
            .then(() => getPercentageThreshold(gDb, deviceId, 'MODULE_ID'))
            .then((threshold) => {
                request(gServer)
                    .get(`/threshold/${deviceId}/MODULE_ID`)
                    .expect(200)
                    .end((err, res) => {
                        if (err) {
                            return done(new Error('expected error to be undefined, got: ' + err));
                        }

                        expect(res.body.threshold).toBe(threshold);
                        done();
                    });
            });
    });
});

afterAll(() => {
    return gDb.close(true);
});

