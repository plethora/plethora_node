/** env jest */

import * as request from 'supertest';
import { Db, MongoClient } from 'mongodb';
import { Server, createServer } from 'restify';
import { v4 } from 'uuid';

import serverFactory from '../src/routes/serverFactory';

const leftpad = (n: number) =>
    n < 10
        ? `0${n}`
        : `${n}`;


let gServer: Server;
let gDb: Db;

const deviceId = v4();
const nowDate = new Date();
const now = nowDate.getTime();
const nowFormatted = leftpad(nowDate.getHours()) + ':' + leftpad(nowDate.getMinutes());
const data = Array.apply(null, Array(40)).map((_: any, i: number) => ({
    device_id: deviceId,
    module_id: 'MODULE_ID',
    timestamp: now - i,
    data: {
        key: 'MODULE_ID',
        value: 32,
    },
}));

beforeAll(() => {
    return MongoClient.connect('mongodb://localhost:27017/PLETHORA_TEST')
        .then((db: Db) => {
            gDb = db;
            gServer = serverFactory(
                createServer(),
                gDb,
            );
            return db.collection('messages')
                .insertMany(data);
        });
});

describe(`/devices/${deviceId}/MODULE_ID/plot`, () => {
    it('should return the last message, plot formatted', (done) => {
        request(gServer)
            .get(`/devices/${deviceId}/MODULE_ID/plot`)
            .end((err, res) => {
                if (err) return done(new Error('err should be undefined, got ' + err));

                expect(res.body.x).toBe(nowFormatted);
                expect(res.body.y).toBe('32.00');
                done();
            });
    });

    it('should return 20 messages, plot formatted', (done) => {
        request(gServer)
            .get(`/devices/${deviceId}/MODULE_ID/plot/time?from=${now - 19}&to=${now}`)
            .end((err, res) => {
                if (err) return done(new Error('err should be undefined, got ' + err));

                expect(res.body.length).toBe(20);
                expect(res.body[0].x).toBe(nowFormatted);
                expect(res.body[0].y).toBe('32.00');
                done();
            });
    });

    describe('error message when from or to are badly formated', () => {
        it('should return 400 when "to" is NaN', (done) => {
            request(gServer)
                .get(`/devices/${deviceId}/MODULE_ID/plot/time/?from=abcd&to=${now}`)
                .expect(400)
                .end((err, res) => {
                    if (err) return done(new Error('err should be undefined, got ' + err));

                    expect(res.body.code)
                        .toBe('BadRequest');
                    expect(res.body.message)
                        .toBe('There is a mistake on the parameter "to" and "from"');
                    done();
                });
        });

        it('should return 400 when "from" is NaN', (done) => {
            request(gServer)
                .get(`/devices/${deviceId}/MODULE_ID/plot/time/?from=${now}&to=abc`)
                .expect(400)
                .end((err, res) => {
                    if (err) return done(new Error('err should be undefined, got ' + err));

                    expect(res.body.code)
                        .toBe('BadRequest');
                    expect(res.body.message)
                        .toBe('There is a mistake on the parameter "to" and "from"');
                    done();
                });
        });
    });

    describe('heartbeat messages', () => {
        const newDeviceId = v4();
        const newNowDate = new Date();
        const newNow = newNowDate.getTime();
        const newNowFormatted =
            leftpad(newNowDate.getHours()) +
            ':' +
            leftpad(newNowDate.getMinutes());

        beforeAll(() => {
            return gDb.collection('messages')
                .insert({
                    device_id: newDeviceId,
                    module_id: 'DEFAULT',
                    timestamp: newNow,
                    data: {
                        key: 'HEARTBEAT',
                        value: 'UP',
                    },
                });
        });

        it('should plot the last heartbeat message', (done) => {
            const url =
                `/devices/${newDeviceId}/DEFAULT/plot/`;

            request(gServer)
                .get(url)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(new Error('erro should be undefined, got: ' + err));

                    expect(res.body.x).toBe(newNowFormatted);
                    expect(res.body.y).toBe(1);
                    done();
                });
        });

        it('should plot heartbeat messages', (done) => {
            const url =
                `/devices/${newDeviceId}/DEFAULT/plot/time?from=${newNow - 10}&to=${newNow}`;

            request(gServer)
                .get(url)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(new Error('err should be undefined, got ' + err));

                    expect(res.body.length).toBe(1);
                    expect(res.body[0].x).toBe(newNowFormatted);
                    expect(res.body[0].y).toBe(1);
                    done();
                });
        });

        afterAll(() => {
            return gDb.collection('messages')
                .remove({
                    module_id: 'DEFAULT',
                });
        });
    });
});

it('should return an error when to < from', (done) => {
    request(gServer)
        .get(`/devices/${deviceId}/MODULE_ID/time?from=${now}&to=${now - 20}`)
        .expect(400)
        .end((err, res) => {
            if (err) return done(new Error('expected err to be undefined, got: ' + err));

            expect(res.body.message).toBe('from should be lower than to');
            done();
        });
});
});

afterAll(() => {
    return gDb.collection('messages').remove({}).then(() => gDb.close(true));
});
