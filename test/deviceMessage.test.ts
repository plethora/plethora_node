/** env jest */

import * as request from 'supertest';
import { Db, MongoClient } from 'mongodb';
import { Server, createServer } from 'restify';
import { v4 } from 'uuid';

import serverFactory from '../src/routes/serverFactory';

const getRandom = () => Math.floor(
    Math.random() * 100,
);

let gServer: Server;
let gDb: Db;

const deviceId = v4();
const now = new Date().getTime();
const data = Array.apply(null, Array(40)).map((_: any, i: number) => ({
    device_id: deviceId,
    module_id: 'MODULE_ID',
    timestamp: now - i,
    data: {
        key: 'MODULE_ID',
        value: getRandom(),
    },
}));

beforeAll(() => {
    return MongoClient.connect('mongodb://localhost:27017/PLETHORA_TEST')
        .then((db: Db) => {
            gDb = db;
            gServer = serverFactory(
                createServer(),
                gDb,
            );
            return db.collection('messages')
                .insertMany(data);
        });
});

describe(`/devices/${deviceId}/MODULE_ID`, () => {
    it('should return the last message', (done) => {
        request(gServer)
            .get(`/devices/${deviceId}/MODULE_ID`)
            .end((err, res) => {
                if (err) return done(new Error('err should be undefined, got ' + err));

                expect(res.body.timestamp).toBe(now);
                done();
            });
    });

    it('should return 20 messages', (done) => {
        request(gServer)
            .get(`/devices/${deviceId}/MODULE_ID/time?from=${now - 19}&to=${now}`)
            .end((err, res) => {
                if (err) return done(new Error('err should be undefined, got ' + err));

                expect(res.body.length).toBe(20);
                expect(res.body[0].timestamp).toBe(now);
                done();
            });
    });
});

afterAll(() => {
    return gDb.collection('messages').remove({}).then(() => gDb.close(true));
});
